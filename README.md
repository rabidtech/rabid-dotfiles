# Rabid dotfiles

* A collection of configuration files for linters and style checkers.
* Used across rabid projects

# Instructions for sticking to rabid styleguides

### 1: Install linters

```sh
# eslint lints javascript
npm install -g eslint

# rubocop lints ruby
gem install rubocop
```

### 2: Put linter configuration files in project root dir

Copy the configuration files from this repo into your project root and commit them

### 3: Setup live linting in your editor

#### 3A: Setting up linting for sublime text

1. Install the sublime package manager [https://packagecontrol.io/](https://packagecontrol.io/)
1. Install the following plugins:
    * `EditorConfig`
    * `SublimeLinter`

#### 3B: Setup live linting in Vim

1. Install `syntastic` plugin [https://github.com/scrooloose/syntastic](https://github.com/scrooloose/syntastic) via whichever Vim plugin manager you prefer

### 4: Setup CI to run the linters as part of your tests for the project

???

# Updating

The updater script will pull down the latest versions of these files and copy them into whatevery directory you run the script from e.g.

```sh
cd /path/to/my/project
curl https://bitbucket.org/rabidtech/rabid-dotfiles/raw/master/update.sh | sh
```

# Setting up simplecov

We configure ruby projects to have a minimum test coverage of 90% - add the following to `spec/spec_helper.rb`

```ruby
# spec/spec_helper.rb

require "simplecov"

##
# Configure minimum test coverage levels
#
# Details of default values for these configuration options can be seen at
# https://github.com/colszowka/simplecov/blob/master/lib/simplecov/configuration.rb#L217
#
SimpleCov.minimum_coverage 90
# SimpleCov.minimum_coverage_by_file 80
# SimpleCov.maximum_coverage_drop 5
# SimpleCov.refuse_coverage_drop

SimpleCov.start do
  add_filter "/spec/support/"
  add_filter "/spec/factories/"
  add_filter "/spec/rails_helper.rb"
  add_filter "/spec/spec_helper.rb"
end
```

