#!/usr/bin/env bash

add_if_not_in_file() {
  touch "$2" # ensure file exists to make grep happy

  grep -q -x -F "$1" "$2" || ( echo "$3" && echo "$1" >> "$2" )
}

echo "-------------------------------------------"
echo "installing rbenv..."

curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-installer | bash

echo "-------------------------------------------"

add_if_not_in_file 'export PATH="$HOME/.rbenv/bin:$PATH"' ~/.bash_profile "updating PATH via .bash_profile..."
add_if_not_in_file 'export PATH="$HOME/.rbenv/bin:$PATH"' ~/.bashrc "updating PATH via .bashrc..."
add_if_not_in_file 'eval "$(rbenv init -)"' ~/.bash_profile "adding rbenv init to .bash_profile..."
add_if_not_in_file 'eval "$(rbenv init -)"' ~/.bashrc "adding rbenv init to .bashrc..."

# has to be done in provision script to prevent errors
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

echo "-------------------------------------------"
echo "running rbenv init..."
rbenv --version
rbenv init

echo "-------------------------------------------"
echo "running rbenv doctor..."

curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-doctor | bash
