#!/usr/bin/env bash

echo ""
echo "-------------------------------------------"
echo "hello world, I'm installing the ruby now..."

cd "$1"

rbenv install

ruby -v

echo "-------------------------------------------"
echo "installing rubocop..."

gem install rubocop
rubocop -v

echo "done!"
# region blah
echo "-------------------------------------------"
# endregion
echo "installing bundler..."

BUNDLE_VERSION=`cat Gemfile.lock | grep -A1 "BUNDLED WITH" | grep -v "BUNDLED WITH"`

echo "Detected bundler version: ${BUNDLE_VERSION}"

gem install bundler -v ${BUNDLE_VERSION} --no-document
bundle -v

echo "done!"
echo "-------------------------------------------"
echo "hello world, I'm installing the ruby now..."

bundle install

# just in case the shebang is wrong (by pointing to ruby.exe instead of just ruby)
ruby ./bin/rails -v

echo "---------------------------------------------------------------"
echo "DETACHING node_modules FROM HOST SHARE FOR BIN SYMLINKS TO WORK"
echo "THIS MEANS ANY CHANGES TO node_modules MUST BE DONE ON GUEST"

# vagrant on windows doesn't let you have symlinks on the guest os
# on shared drives, unless you use :smf which requires admin.
# instead, we create a new folder, which is mounted as a binding
# + this gives a massive speed boost, on top of letting everything work
# - any changes you make to node_modules on host are not reflected on guest

mkdir $1/node_modules
sudo mkdir /vagrant_node_modules # doesn't matter if it already exists
sudo chown "$USER" /vagrant_node_modules # give folder to the standard user
#sudo chmod o+w /vagrant_node_modules # let "standard" users write to folder

# this doesn't seem to work, due to vagrant shares being mounted *after* the system comes up
#add_if_not_in_file "/vagrant_node_modules /vagrant/node_modules none defaults,bind 0 0" /etc/fstab "Adding vagrant_node_modules binding to fstab..."
#add_if_not_in_file "~/vagrant_node_modules" /etc/fstab "Adding vagrant_node_modules binding to fstab..."

# mount it for use right now
sudo mount --bind /vagrant_node_modules $1/node_modules

yarn install --check-files
