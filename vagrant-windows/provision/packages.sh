#!/usr/bin/env bash

echo "-------------------------------------------"
echo "installing suggested build environment..."

# postgresql-server-dev-X.Y

# setup apt-get so it can install google-chrome-stable
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'

# run script to install NodeSource Node.js 10.x repo for apt-get
sudo curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -

# setup apt-get so it can install yarn
sudo curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
sudo echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt-get update -q && sudo apt-get install -y -q --no-install-recommends \
    redis-server \
    postgresql \
    postgresql-contrib \
    imagemagick \
    autoconf \
    bison \
    build-essential \
    libmysqlclient-dev \
    libssl1.0-dev \
    libyaml-dev \
    libreadline6-dev \
    zlib1g-dev \
    libncurses5-dev \
    libffi-dev \
    libgdbm5 \
    libgdbm-dev \
    libpq-dev \
    sqlite3 \
    libsqlite3-dev \
    google-chrome-stable \
    nodejs \
    yarn

# sudo -u postgres bash -c "psql -c \"CREATE USER vagrant WITH PASSWORD 'vagrant';\""
sudo -u postgres psql -U postgres -d postgres -c "alter user postgres with password 'postgres';"

# enable postgresql
update-rc.d postgresql enable

echo "-------------------------------------------"
echo "displaying versions of common packages..."

redis-server -v
imagemagick --version
sqlite3 -version
gcc --version
psql --version
node -v
npm -v
yarn -v
