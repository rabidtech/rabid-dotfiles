# Vagrant for running Ruby/Rails projects on Windows

This folder contains the files needed to create a Vagrant for a standard Ackama rails project.
There is minimal configuration needed - it should just work straight out of the box.

Just copy the `Vagrantfile` & `provision` folder over into the root of your project, and run `vagrant up`.

This requires vagrant 2.1.0 or above, as it uses [triggers](https://www.vagrantup.com/docs/triggers/).

The `Vagrantfile` (and related provision scripts) should result in a vagrant VM supporting:
 - rbenv
 - redis-server
 - postgresql
 - imagemagick
 - sqlite3
 - nodejs
 - yarn
 
 and a few other goodies that you may or may not need.
 
It's recommended that you actually *use* this Vagrant, rather than just as a blackbox for development.

This means using `vagrant ssh` to connect to the Vagrant.

# Important points

### `vagrant ssh` on Windows 10 (VAGRANT_PREFER_SYSTEM_BIN)

If you're on Windows 10, with the latest version of Vagrant, you might run into the following:

```cmd
> vagrant ssh
vagrant@127.0.0.1: Permission denied (publickey).
```

This is because Windows 10 includes its own SSH client, which doesn't work.

The solution to this is to not use the local Windows SSH client, by setting the global env variable `VAGRANT_PREFER_SYSTEM_BIN` to `0`.

This env variable quite likely won't exist, so you'll have to make it.

### postgres

The `postgres` user in `psql` has it's password set to `postgres`.

This should be fine, and not needed to be questioned at all.

### Bundle(r) version
The `bundle` gem tracks the version of bundle that generated `Gemfile.lock`, like so:

```rbenv-gemsets
BUNDLED WITH
   2.0.1
```

Due to a [bug with bundle](https://bundler.io/blog/2019/01/04/an-update-on-the-bundler-2-release.html), bundle 2 will have issues if the `BUNDLED WITH`
version isn't `>= 2`. As a result, when running `bundle install` with projects you might get this error:

```text
Can't find gem bundler (>= 0.a) with executable bundle (Gem::GemNotFoundException)
```

To wkr around this, he `app.sh` provision script will attempt to extract the `BUNDLED WITH` version, and install that version of `bundler`.
However, it's very possible that that could fail - if it does, just hardcode the bundle version in the `app.sh` file.

You should always keep an eye on the version of bundle you're using, and the `BUNDLED WITH` property value in your projects `Gemfile.lock`.

### node_modules

`node_modules` is handled via a [`mount --bind`](https://unix.stackexchange.com/questions/198590/what-is-a-bind-mount) in `fstab`.

Vagrant by default shares the folder the `Vagrantfile` is in as a folder located at `/vagrant`.
This means that any changes on the Host OS to that folder is also seen by the Guest OS.

This creates a problem however, as some npm packages are platform specific (looking at you `node-sass`).
This means that when you run `yarn install` on Windows, platform specific packages will have their *Windows* binaries installed,
causing them to fail when you try and use them on the Linux OS.

However, if you run `yarn install` on Linux on the Guest VM, it'll fail as Vagrant doesn't let you create symlinks on shared drives.

(The reason behind this is complex - there are ways around this, but they usually all result in having to run `vagrant up` in an elevated command prompt, which is itself a whole different pain)

The solution to this is to use `mount --bind` to "redirect" the `node_modules` folder to another folder on the Guest OS.

#### The good news
Everything is now *really* fast, as working with `node_modules` no longer results in *any* I/O between the Host & Guest OS'.

#### The bad news
The `node_modules` of Host & Guest OS' will not be kept in sync.

This means that if you want to make any changes to the contents of `node_modules` (such as, and most commonly, installing a new package or updating an existing one),
you have to do it in the Vagrant itself. The commends are exactly the same, you just have to run them inside the vagrant. 

Note that this mounting is done using a Vagrant [trigger](https://www.vagrantup.com/docs/triggers/).
This is because the vagrant shared folders are mounted after system boot, and so after fstab has run.

### Using webpack-dev-server

Modern Rails uses `webpacker` to compile its content.

If you run just the rails server (via `rails server`), when you load a page, webpack will be compiled if it's needed.
This is fine, but also somewhat slow. You can instead run `webpack-dev-server` to speed up the compiling using automatic recompiling & hot reloading.

This is done by running `rails ./bin/webpack-dev-server` from the commandline, from inside the vagrant.

However, to get it actually working with Windows, a few changed need to be made to the configuration files of rails:

##### config/webpacker.yml

The `host` & `public` (under `development.dev_server`) settings need to be changed from `localhost` to `0.0.0.0`:

```yaml
# ...
development:
  # ...
  dev_server:
    # ...
    host: 0.0.0.0
    port: 3035
    public: 0.0.0.0:3035
    # ...
  # ...
# ...
```


### RubyMine

To set things up properly with `RubyMine`, you'll want to add the Vagrant as a remote ruby SDK.

The ruby path should be: `home\vagrant\.rbenv` - if you're asked to pick a ruby version, make sure to pick the same one the project is using! 

Finally, with this done, make sure that the sdk the *module* is using is also set to the remote ruby sdk.
