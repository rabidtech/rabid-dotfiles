#!/usr/bin/env bash
set -e

rvm use $(cat .ruby-version) --install
bundle install

# Install and update bundle-audit
gem install bundler-audit
