bundle exec rake spec
bundle exec rubocop
bundle-audit --update
bundle exec brakeman --run-all-checks --exit-on-warn .
